#!/bin/bash
# induce artificial wifi stress to a linux round-robin-host student server
# the following assumes ssh keys are setup to avoid any prompts, yes I could use yes and no
# Test conditions: script would be run while on a wifi network on a mobile device that can connect 
# to student server
# Since linux.student.cs itself is on an independent network, you would want to create multiple persistent
# ssh connections on the initiating device. Hmm ...

s="0"

while [ $s -lt 4 ]
do
	ssh -X omnafees@linux.student.cs xclock &
	s=$[$s+1]
	sleep 0.5
done
